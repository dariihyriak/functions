const getSum = (str1, str2) => {
  debugger
  if (typeof str1 !== 'string' || typeof str2 !== 'string') {
    return false;
  }
  if (str1.length === 0) {
    if(!Number(str2)){
      return false;
    }
    return str2
  }
  if (str2.length === 0) {
    if(!Number(str1)){
      return false;
    }
    return str1
  }
  if (!Number(str1) || !Number(str2)) {
    return false;
  }

  return (+str1 + +str2).toString();
};

const getQuantityPostsByAuthor = (listOfPosts, authorName) => {
  let postCounter = 0;
  let commentCounter = 0;
  listOfPosts.forEach(obj => {
    if(obj.author === authorName){
      postCounter++;
    }
    if('comments' in obj){
      obj.comments.forEach(comment => {
        if(comment.author === authorName){
          commentCounter++;
        }
      })
    }
  });
  return `Post:${postCounter},comments:${commentCounter}`
};

const tickets=(people)=> {
  if(people[0] !== 25){
    return "NO"
  }
  let counter = 0;

  for(let i = 0; i < people.length; i++){
    const change = +people[i] - +people[i-1];
    if(change > 25 && change - counter > 0){
      return 'NO';
    }
    counter += change === 0 ? 25 : 100
  }
  return "YES";
};


module.exports = {getSum, getQuantityPostsByAuthor, tickets};
